﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public List<PowerUp> powerUpList; 
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            ActivatePowerUp();
            this.gameObject.SetActive(false);
        }
    }

    //Activate Random Powerup
    public void ActivatePowerUp()
    {
        int totalWeighting = 0;
        //Add Weightings
        foreach(PowerUp pu in powerUpList)
        {
            totalWeighting += pu.weighting;
        }
        //Chance of selection purely based on weighting
        foreach(PowerUp pu in powerUpList)
        {
            if(Random.Range(0, totalWeighting) <= pu.weighting)
            {
                pu.PowerUpFunction();
            }
            else
            {
                totalWeighting -= pu.weighting;
            }
        }
    }
}
