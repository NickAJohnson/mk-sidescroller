﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
/// <summary>
/// Game Manager for Play Speed, Game States and scoring
/// </summary>

public class GameManager : MonoBehaviour
{
    //Public Game Options
    public float gameSpeedCap = 3;
    public float gameSpeedIncreaseRate = 1;
    public float platformChance = .05f;
    public float scorePerSecond = 100;
    public float scoreMultiplierPerSecond = 0.005f;
    [Tooltip("Based on GameSpeed")]
    public List<float> difficultyIncreaseMilestones;

    //Public References
    public GameObject ground;
    public GameObject wall;
    public Canvas uiHolder;
    public UI gameOverTransitionUI;
    //UI
    [Tooltip("UI Game will start it")]
    public UI activeUI;
    public TextMeshProUGUI currentScore;
    public List<UI> uiList;
    public TextMeshProUGUI hightScoreTextLeft;
    public TextMeshProUGUI hightScoreTextRight;

    [HideInInspector]
    public float gameSpeed;
    protected float playerScore = 0;

    //GameStates
    //Bool for A game currently being played
    [HideInInspector]
    public bool isPlaying = false;
    [HideInInspector]
    public bool isPaused = false;

    //References
    protected Player player;
    protected Material groundMaterial;
    protected Material wallMaterial;
    protected WallSpawner wallSpawner;
    protected int difficultyTracker = 0;

    //Initialisations
    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        groundMaterial = ground.GetComponent<MeshRenderer>().material;
        wallMaterial = wall.GetComponent<MeshRenderer>().material;
        wallSpawner = GetComponent<WallSpawner>();
    }

    private void Start()
    {
        foreach(UI ui in uiList)
        {
            ui.gameObject.SetActive(false);
        }
        activeUI.gameObject.SetActive(true);
        UpdateHighScoreText();
        //Set Values Ready for GameStart

        NewGame();
    }

    // Update is called once per frame
    private void Update()
    {
        activeUI.ActiveUIUpdate();
        UpdateGameSpeed();

        if (isPlaying)
        {

            //Score Update
            playerScore += scorePerSecond * gameSpeed * Time.deltaTime + playerScore * scoreMultiplierPerSecond * Time.deltaTime;
            currentScore.text = playerScore.ToString("F0");
        }

    }

    protected void UpdateGameSpeed()
    {
        //Increase gamespeed by a percentage of remaining amount to gameSpeedCap
        gameSpeed += (gameSpeedCap - gameSpeed) * (gameSpeedIncreaseRate / 100.0f) * Time.deltaTime;
        //Update speed of objects to match gamespeed
        groundMaterial.SetFloat("_Speed", gameSpeed);
        wallMaterial.SetFloat("_Speed", gameSpeed);
        player.animator.speed = gameSpeed;

        if(difficultyIncreaseMilestones.Count > 0)
        {
            if(gameSpeed > difficultyIncreaseMilestones[difficultyTracker])
            {
                //Set Tracker
                if(difficultyTracker < difficultyIncreaseMilestones.Count -1)
                {
                    difficultyTracker++;
                }
                else
                {
                    //If end of list reached, make sure it stops calling
                    difficultyIncreaseMilestones[difficultyTracker] = float.MaxValue;
                }

                if (wallSpawner.safeWallSpawnCount > 1)
                    wallSpawner.safeWallSpawnCount--;
            }
        }
    }

    #region GamePlay Functions
    public void StartGame()
    {
        //Reseting values for a new game
        isPlaying = true;
        isPaused = false;
        player.isAlive = true;
        playerScore = 0;
        gameSpeed = 1;
        difficultyTracker = 0;
        currentScore.text = playerScore.ToString("F0");
        groundMaterial.SetFloat("_Speed", gameSpeed);
        player.ResetPlayer();
        wallSpawner.ResetSettings();
    }

    public void PauseGame()
    {
        //Allows same function to be used on Pause Button
        if(!isPaused)
        {
            //Paused Function
            Time.timeScale = 0;
            isPaused = true;
        }
        else
        {
            ResumeGame();
        }
    }

    public void ResumeGame()
    {
        isPaused = false;
        Time.timeScale = 1;
    }

    public void EndGame()
    {
        isPlaying = false;
        ActivateNewUI(gameOverTransitionUI);
        SubmitScore(playerScore);
        Time.timeScale = 1;
    }

    public void NewGame()
    {
        groundMaterial.SetFloat("_Speed", 0);
    }
    #endregion

    #region UserInterfaceFunctions

    //Replace ActiveUI with given UI
    public void ActivateNewUI(UI newActiveUI)
    {
        //Set Previous UI for return buttons
        newActiveUI.previousUI = activeUI;
        foreach(UI ui in uiList)
        {
            ui.gameObject.SetActive(false);
        }
        newActiveUI.gameObject.SetActive(true);
        activeUI = newActiveUI;
    }

    //Activate new UI but have previous UI visible
    public void ActivateOnTopOfUI(UI newActiveUI)
    {
        //Set Previous UI for return buttons
        newActiveUI.previousUI = activeUI;
        activeUI = newActiveUI;
        activeUI.gameObject.SetActive(true);
    }

    //Swap active UI to stored previousUI
    public void GoBackUI()
    {
        activeUI.gameObject.SetActive(false);
        activeUI = activeUI.previousUI;
        activeUI.gameObject.SetActive(true);
    }

    #endregion

    #region Scores
    public void IncreaseScore(float percentIncrease)
    {
        playerScore *= percentIncrease + 1;
        currentScore.text = playerScore.ToString("F0");
    }

    public void SubmitScore(float newScore)
    {
        float scoreHolder = 0;
        //Check if given score is a HighScore
        if(newScore > PlayerPrefs.GetFloat("HS10", 0))
        {
            //start at 1 for clarity
            for(int i = 1; i < 11; i++)
            {
                if (PlayerPrefs.GetFloat("HS" + i.ToString(), 0) > playerScore)
                {
                    //Continue function if score at i > newScore
                    continue;
                }
                else
                {
                    //replace old score and continue with old score - will iterate and update rest of list
                    scoreHolder = PlayerPrefs.GetFloat("HS" + i.ToString(), 0);
                    PlayerPrefs.SetFloat("HS" + i.ToString(), playerScore);
                    playerScore = scoreHolder;
                }
            }
            UpdateHighScoreText();
        }
    }

    public void ClearHighScores()
    {
        //start at 1 for clarity
        for (int i = 1; i < 11; i++)
        {
            PlayerPrefs.SetFloat("HS" + i.ToString(), 0);
        }
        UpdateHighScoreText();
    }

    public void UpdateHighScoreText()
    {
         hightScoreTextLeft.text =
     "1st - " + PlayerPrefs.GetFloat("HS1", 0).ToString("F0")
     + "\n2nd - " + PlayerPrefs.GetFloat("HS2", 0).ToString("F0")
     + "\n3rd - " + PlayerPrefs.GetFloat("HS3", 0).ToString("F0")
     + "\n4th - " + PlayerPrefs.GetFloat("HS4", 0).ToString("F0")
     + "\n5th - " +PlayerPrefs.GetFloat("HS5", 0).ToString("F0");

        hightScoreTextRight.text =
     "6th - " + PlayerPrefs.GetFloat("HS6", 0).ToString("F0")
     + "\n7th - " + PlayerPrefs.GetFloat("HS7", 0).ToString("F0")
     + "\n8th - " + PlayerPrefs.GetFloat("HS8", 0).ToString("F0")
     + "\n9th - " + PlayerPrefs.GetFloat("HS9", 0).ToString("F0")
     + "\n10th - " + PlayerPrefs.GetFloat("HS10", 0).ToString("F0");
    }
    #endregion


}