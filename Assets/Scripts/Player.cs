﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Vector3 deathForceVector;
    public float jumpHeight; //Change to jump height and calculate the velocity required
    public float fallMultiplier = 2.5f;
    public float slowFallRateMultiplier = .5f;
    public float invulnerabilityDuration = 3.0f;
    public GameObject shieldObject;

    [HideInInspector]
    public bool isAlive;
    protected bool isInJump = false;
    protected bool isShieldActive = false;
    protected bool isInvulnerable = false;
    protected float invulnerabiltyTimer = 0.0f;

    [HideInInspector]
    public Animator animator;
    protected Rigidbody rb;
    protected GameManager gameManager;

    private void Awake()
    {
        animator = this.GetComponentInChildren<Animator>();
        rb = this.GetComponent<Rigidbody>();
        gameManager = GameObject.FindObjectOfType<GameManager>();
    }

    void Start()
    {
        shieldObject.SetActive(false);
        
    }

    // Update is called once per frame
    void Update()
    {
        //Player alive functions
        if(isAlive)
        {
            //Faster falling for better feel
            if(rb.velocity.y < 0)
            {
                if (Input.GetKey(KeyCode.Space) || Input.GetMouseButton(0) || Input.touchCount != 0)
                {
                    //Holding jump provides floating effect
                    rb.velocity += (Vector3.up * Physics.gravity.y * (slowFallRateMultiplier -1) * Time.deltaTime);
                }
                else
                {
                    rb.velocity += (Vector3.up * Physics.gravity.y * (fallMultiplier - 1) * Time.deltaTime);
                }
            }

            //Keyboard Controls
            if (Input.GetKey(KeyCode.Space))
            {
                Jump();
            }

            if (Input.GetKeyDown(KeyCode.W))
            {
                MoveUpLane();
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                MoveDownLane();
            }
        }



        if(isInvulnerable)
        {
            invulnerabiltyTimer -= Time.deltaTime;
            if(invulnerabiltyTimer < 0.0f)
            {
                isInvulnerable = false;
            }
        }



    }

    public void OnDeath()
    {
        //Ensuring player only dies once
        if(isAlive && !isInvulnerable)
        {
            if(isShieldActive)
            {
                isShieldActive = false;
                shieldObject.SetActive(false);
                //Set invulnerable and start timer
                isInvulnerable = true;
                invulnerabiltyTimer = invulnerabilityDuration;
                return;
            }
            isAlive = false;
            animator.enabled = false;
            if(gameManager.isPlaying)
            {
                gameManager.EndGame();
            }
            foreach (Rigidbody rb in GetComponentsInChildren<Rigidbody>())
            {
                rb.AddForce(deathForceVector);
            }
            
        }
    }

    public void Jump()
    {
        if(!isInJump)
        {
            isInJump = true;
            animator.SetBool("InJump", true);
            //Calculate velocity required from height input
            rb.velocity = Vector3.up * Mathf.Sqrt(2 * jumpHeight * (Physics.gravity.y * -1));
        }
    }

    public void MoveUpLane()
    {
        //Check if player is on edge lane
        if (transform.position.z < 1.5f)
        {
            transform.position += Vector3.forward * 1;
        }
    }

    public void MoveDownLane()
    {
        if (transform.position.z > -1.5f)
        {
            transform.position += Vector3.back * 1;
        }
    }



    public void ActivateShield()
    {
        isShieldActive = true;
        shieldObject.SetActive(true);
    }

    //Function for placing player back on start area
    public void ResetPlayer()
    {
        transform.position = Vector3.zero;
        animator.enabled = true;
        rb.velocity = Vector3.zero;
        //limbs of the player must be close to the animator for the player to return to running
        foreach (Rigidbody rb in transform.GetComponentsInChildren<Rigidbody>())
        {
            rb.transform.localPosition = Vector3.zero;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Ground")
        {
            animator.SetBool("InJump", false);
            isInJump = false;
        }
    }

    //Dies if hitting a Ground trigger box
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ground")
        {
            OnDeath();
            other.gameObject.SetActive(false);
        }
    }
}
