﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Wall spawner generates and moves walls towards the player
public class WallSpawner : MonoBehaviour
{
    //Wall Settings
    public int laneCount;
    public float distanceBetweenWalls = 20;
    public float wallSpeed = 8;
    public Transform wallResetMarker;

    //Prefabs
    public GameObject wallPrefab;
    public GameObject tilePrefab;

    //References
    protected GameManager gameManager;
    protected Player player;
    protected GameObject ground;
    protected List<Wall> wallList = new List<Wall>();
    protected int totalWalls = 4;
    //store initial amount for game resets
    protected int initialSafeWallSpawnCount;

    //[HideInInspector]
    public int safeWallSpawnCount;

    //Initialisations
    private void Awake()
    {
        initialSafeWallSpawnCount = safeWallSpawnCount;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        ground = GameObject.FindGameObjectWithTag("Ground");

        //Initialise Walls from player location - Starting at index 1 for easier location calculation
        for (int i = 1; i < totalWalls + 1; i++)
        {
            Wall newWall = Instantiate(wallPrefab).GetComponent<Wall>();
            wallList.Add(newWall);
            newWall.transform.position = new Vector3(distanceBetweenWalls * i + player.transform.position.x, tilePrefab.transform.localScale.y / 2, 0);
            newWall.Initialise(laneCount, safeWallSpawnCount);
        }

    }
    void Start()
    {
        //Set Width of Ground based on Starting Lanes
        ground.transform.localScale = new Vector3(ground.transform.lossyScale.x, ground.transform.lossyScale.y, laneCount);
    }

    // Update is called once per frame
    void Update()
    {
        foreach(Wall w in wallList)
        {
            w.transform.position += Vector3.left * (gameManager.gameSpeed * wallSpeed * Time.deltaTime);
            if(w.transform.position.x < wallResetMarker.position.x)
            {
                //Reuse wall once past player
                ResetWall(w);
            }
        }
    }

    public void ResetWall(Wall wall)
    {
        //Shift only along the x by a set amount to keep constant distance between walls
        wall.transform.position += new Vector3(distanceBetweenWalls * totalWalls + player.transform.position.x, 0, 0);
        wall.ReuseWall(laneCount, safeWallSpawnCount);
    }

    public void ResetSettings()
    {
        safeWallSpawnCount = initialSafeWallSpawnCount;
        for( int i = 0; i < totalWalls; i++)
        {
            wallList[i].transform.position = new Vector3(distanceBetweenWalls * (i+1) + player.transform.position.x, tilePrefab.transform.localScale.y / 2, 0);
            wallList[i].ReuseWall(laneCount, safeWallSpawnCount);
        }
    }


}
