﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public GameObject platform;
    public GameObject item;
    public Material safeMaterial;
    public Material harmfulMaterial;

    protected bool isHarmful = false;
    protected Renderer renderer;
    protected GameManager gameManager;

    private void Awake()
    {
        renderer = this.GetComponent<Renderer>();
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        platform.SetActive(false); 
    }

    public void SetTile(bool newIsHarmful)
    {
        if (Random.Range(0.0f, 1.0f) < gameManager.platformChance)
        {
            //Activate platform
            platform.SetActive(true);
            item.SetActive(true);
        }
        else
        {
            //Deactivate platform
            platform.SetActive(false);
            item.SetActive(false);
        }

        if(newIsHarmful)
        {
            //Set to Harmful Tile
            isHarmful = true;
            renderer.material = harmfulMaterial;
        }
        else
        {
            isHarmful = false;
            renderer.material = safeMaterial;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(isHarmful)
        {
            other.GetComponentInParent<Player>().OnDeath();
        }
        else
        {
            //dissolve shader effect - to be implemented
        }

    }
}
