﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    [HideInInspector]
    public List<Tile> tileList = new List<Tile>();
    public GameObject tilePrefab;

    private int safeTilesSpawned;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Creates and places Tiles
    public void Initialise(int totalTileCount, int safeTileCount)
    {
        safeTilesSpawned = 0;
        for (int i = 0; i < totalTileCount; i++)
        {
            //Create and Place New Tile
            Tile newTile = Instantiate(tilePrefab, this.transform).GetComponent<Tile>();
            tileList.Add(newTile);
            newTile.transform.localPosition = new Vector3(0, 0, i - ((float)totalTileCount - 1)/2);

            //Randomises Safe Tiles, returns true if safe
            SetTileState(safeTileCount, totalTileCount, i);
        }
    }

    public void ReuseWall(int totalTileCount, int safeTileCount)
    {
        //Check if using same amount of tiles
        if(totalTileCount == tileList.Count)
        {
            safeTilesSpawned = 0;
            for (int i = 0; i < totalTileCount; i++)
            {
                SetTileState(safeTileCount, totalTileCount, i);
            }
        }
        else
        {
            //TODO Resize Tile List if different amount of tiles
            tileList.Clear();
        }
    }

    //Set the Tile state
    public void SetTileState(int safeTilesTotal, int totalTileCount, int index)
    {
        bool isSafeTile = false;
        //Each Tile must roll for each remaining safeTile to be spawned to ensure an even spread of safe tiles
        for (int j = 0; j < safeTilesTotal - safeTilesSpawned; j++)
        {
            //Randomises SafeTileSpawn locations but ensures the full amount of safe tiles are spawned
            if ((Random.Range(0, totalTileCount - index) < 1 && safeTilesTotal - safeTilesSpawned > 0) || safeTilesTotal >= totalTileCount + safeTilesSpawned - index)
            {
                isSafeTile = true;
                break;
            }
        }
        if (isSafeTile)
        {
            //Spawn a Safe Tile
            tileList[index].SetTile(false);
            safeTilesSpawned++;
        }
        else
        {
            tileList[index].SetTile(true);
        }
    }
}
