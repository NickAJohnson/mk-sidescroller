﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Based on Swipe Mobile Controls Tutorial by N3K EN - https://www.youtube.com/watch?v=rDK_3qXHAFg
/// Works for Mobile and Mouse
/// </summary>


public class MobileControls : MonoBehaviour
{
    private bool isDragging = false;
    private Vector2 startTouch, swipeDelta;
    private bool tap, swipeUp, swipeDown = false;
    //bool for checking if tap or swipe
    private bool isSwipe = false;
    private Player player;
    private GameManager gameManager;
    private bool isActive = false;
    public bool testOnPC = false;

    private void Awake()
    {
        player = GetComponent<Player>();
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        //Delays updating the frame game resumes to prevent jumping on UI button presses
        if(!gameManager.isPlaying || gameManager.isPaused)
        {
            isActive = false;
        }
        if (!isActive && gameManager.isPlaying && !gameManager.isPaused)
        {
            isActive = true;
            return;
        }
        if(isActive)
        {
            if(testOnPC)
            {
                //Standalone Input
                if(Input.GetMouseButtonDown(0))
                {
                    tap = true;
                    startTouch = Input.mousePosition;
                    isDragging = true;
                }
                else if(Input.GetMouseButtonUp(0))
                {
                    CheckInputs();
                    PerformAction();
                    ResetValues();
                }
            }
            else
            {
                //Mobile Inputs
                if(Input.touches.Length != 0)
                {
                    if(Input.touches[0].phase == TouchPhase.Began)
                    {
                        tap = true;
                        startTouch = Input.touches[0].position;
                        isDragging = true;
                    }
                    else if(Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
                    {
                        if(Input.touches[0].phase == TouchPhase.Ended)
                        {
                            CheckInputs();
                            PerformAction();
                        }
                        ResetValues();
                    }
                }
            }
        }
    }

    private void CheckInputs()
    {
        //calculate the distance
        swipeDelta = Vector2.zero;
        if (isDragging)
        {
            if(testOnPC)
            {
                if (Input.GetMouseButtonUp(0))
                {
                    swipeDelta = (Vector2)Input.mousePosition - startTouch;
                }
            }
            else
            {
                if (Input.touches.Length != 0)
                {
                    swipeDelta = Input.touches[0].position - startTouch;
                }

            }
        }

        //Did we Cross Deadzone
        if (swipeDelta.magnitude > 100)
        {
            if (swipeDelta.y > 0)
            {
                swipeUp = true;
                isSwipe = true;
            }
            else
            {
                swipeDown = true;
                isSwipe = true;
            }
        }
    }

    private void PerformAction()
    {
        if(swipeUp)
        {
            player.MoveUpLane();
            return;
        }
        else if(swipeDown)
        {
            player.MoveDownLane();
            return;
        }
        else if(!isSwipe)
        {
            player.Jump();
        }
    }

    private void ResetValues()
    {
        startTouch = Vector3.zero;
        swipeDelta = Vector3.zero;
        isDragging = false;
        isSwipe = swipeUp = swipeDown = false;
    }
}
