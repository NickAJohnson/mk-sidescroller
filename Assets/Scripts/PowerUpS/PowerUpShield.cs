﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewShieldPowerUp", menuName = ("ScriptableObject/PowerUp/Shield"))]
public class PowerUpShield : PowerUp
{
    public override void PowerUpFunction()
    {
        Debug.Log("Shield");
        GameObject.FindObjectOfType<Player>().ActivateShield();
    }
}
