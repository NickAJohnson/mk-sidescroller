﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Gives player bonus score for collecting
/// </summary>
[CreateAssetMenu(fileName = "NewScorePowerUp", menuName = ("ScriptableObject/PowerUp/ScoreBoost"))]
public class ScorePowerUp : PowerUp
{
    public float percentageScoreIncrease = 10;

    public override void PowerUpFunction()
    {
        GameObject.FindObjectOfType<GameManager>().IncreaseScore(percentageScoreIncrease/100);
    }
}
