﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PowerUp : ScriptableObject
{
    [Tooltip("Variable rarity of powerup, lower weighting = less chance")]
    public int weighting;
    public abstract void PowerUpFunction();
}
