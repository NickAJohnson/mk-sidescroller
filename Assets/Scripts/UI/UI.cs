﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UI : MonoBehaviour
{
    [HideInInspector]
    public UI previousUI;
    //Allows a UI to hold a function that can be executed by a ConfirmationMenuUI
    public delegate void StoreFunction();
    [HideInInspector]
    public StoreFunction storedFunction;

    abstract public void ActiveUIUpdate();
}
