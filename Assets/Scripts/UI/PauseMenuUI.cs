﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuUI : UI
{
    private GameManager gameManager;
    private Player player;
    public ConfirmationMenuUI quitTransitionUI;
    public UI quitConfirmedTransitionUI;

    public void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        storedFunction = QuitConfirmed;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
    }

    public override void ActiveUIUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ResumeButtonPress();
        }
    }

    public void ResumeButtonPress()
    {
        gameManager.ResumeGame();
        gameManager.GoBackUI();
    }

    public void QuitButtonPress()
    {
        gameManager.ActivateOnTopOfUI(quitTransitionUI);
    }

    public void QuitConfirmed()
    {

        gameManager.EndGame();
        gameManager.ActivateNewUI(quitConfirmedTransitionUI);
    }
}
