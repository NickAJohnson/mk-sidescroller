﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighScoresUI : UI
{
    private GameManager gameManager;
    public ConfirmationMenuUI clearTransitionUI;

    private void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        storedFunction = gameManager.ClearHighScores;
        storedFunction += gameManager.GoBackUI;
    }

    public override void ActiveUIUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            BackButtonPress();
        }
    }

    public void BackButtonPress()
    {
        gameManager.GoBackUI();
    }

    public void ClearButtonPress()
    {
        gameManager.ActivateOnTopOfUI(clearTransitionUI);
    }
}

