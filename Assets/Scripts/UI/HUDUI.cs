﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDUI : UI
{
    private GameManager gameManager;
    public UI pauseTransitionUI;

    public void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
    }

    public override void ActiveUIUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseButtonPress();
        }
    }

    public void PauseButtonPress()
    {
        gameManager.ActivateOnTopOfUI(pauseTransitionUI);
        gameManager.PauseGame();
    }
}
