﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverUI : UI
{
    private GameManager gameManager;
    [Tooltip("UI for Start to open")]
    public UI mainMenuTransitionUI;
    public UI playAgainTransitionUI;
    public UI highScoresTransitionUI;


    public void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
    }

    public override void ActiveUIUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            MainMenuButtonPress();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PlayAgainButtonPress();
        }
    }

    public void PlayAgainButtonPress()
    {
        gameManager.NewGame();
        gameManager.StartGame();
        gameManager.ActivateNewUI(playAgainTransitionUI);
    }

    public void MainMenuButtonPress()
    {
        gameManager.NewGame();
        gameManager.ActivateNewUI(mainMenuTransitionUI);
    }

    public void HighScoresButtonPress()
    {
        gameManager.ActivateOnTopOfUI(highScoresTransitionUI);
    }
}
