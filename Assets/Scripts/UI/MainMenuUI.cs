﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuUI : UI
{
    private GameManager gameManager;
    [Tooltip("UI for Start to open")]
    public UI startTransitionUI;
    public UI highScoresTransitionUI;


    public void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
    }

    public override void ActiveUIUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            StartButtonPress();
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            QuitButtonPress();
        }
    }

    public void StartButtonPress()
    {
        gameManager.StartGame();
        gameManager.ActivateNewUI(startTransitionUI);
    }

    public void QuitButtonPress()
    {
        Application.Quit();
    }

    public void HighScoresButtonPress()
    {
        gameManager.ActivateOnTopOfUI(highScoresTransitionUI);
    }
}
