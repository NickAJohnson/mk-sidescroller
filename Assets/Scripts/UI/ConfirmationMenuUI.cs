﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfirmationMenuUI : UI
{
    private GameManager gameManager;

    public void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
    }

    public override void ActiveUIUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            YesButtonPress();
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            NoButtonPress();
        }
    }

    public void YesButtonPress()
    {
        //Invoke the stored function of the UI that called this
        previousUI.storedFunction.Invoke();
    }

    public void NoButtonPress()
    {
        gameManager.GoBackUI();
    }
}
